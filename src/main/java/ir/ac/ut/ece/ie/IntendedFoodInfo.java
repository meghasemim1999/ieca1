package ir.ac.ut.ece.ie;

public class IntendedFoodInfo {
    private String foodName;
    private String restaurantName;
    private boolean isRegular;

    public IntendedFoodInfo() {
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }


    public boolean isRegular() {
        return isRegular;
    }

    public void setRegular(String regular) {
        isRegular = regular.equals("true");
    }
}
