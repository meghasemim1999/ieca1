package ir.ac.ut.ece.ie.controllers;

import ir.ac.ut.ece.ie.LoghmehCore;
import ir.ac.ut.ece.ie.exceptions.LackOfUserException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/IncreaseCredit")
public class IncreaseCreditController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String extraCredit = request.getParameter("extraCredit");
        try {
            LoghmehCore.getInstance().increaseCredit(extraCredit);
        } catch (LackOfUserException lackOfUserException) {
            return;
            }
        response.sendRedirect("GetUserProfile?id=" + request.getParameter("id"));
    }
}
