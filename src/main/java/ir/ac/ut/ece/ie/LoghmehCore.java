package ir.ac.ut.ece.ie;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.ie.exceptions.*;
import ir.ac.ut.ece.ie.repositories.*;
import ir.ac.ut.ece.ie.runnables.AssignDeliveryToOrders;

import java.util.*;

import static java.lang.Math.pow;

//import java.net.URI;
//import java.net.http.HttpClient;
//import java.net.http.HttpRequest;
//import java.net.http.HttpResponse;

public class LoghmehCore {
    private static LoghmehCore instance;
    private ObjectMapper mapper;
    private List<OrderedFoodInfo> cart;
    private String orderOnGoingRestaurant;
    private int loggedInUserIndex = 0;

    public String getOrderOnGoingRestaurant(){
        return orderOnGoingRestaurant;
    }

    public int getLoggedInUserIndex(){
        return loggedInUserIndex;
    }

    private static final int MAXIMUM_DISTANCE_ALLOWED = 170;

    private LoghmehCore(){
        mapper = new ObjectMapper();
        cart = new ArrayList<>();
        orderOnGoingRestaurant = "";
    }

    public static void main(String[] args) throws LackOfAnyRestaurantException {
        LoghmehCore.getInstance().getRestaurants();
    }

    public static LoghmehCore getInstance(){
        if(instance == null)
            instance = new LoghmehCore();

        return instance;
    }

    public User getUserProfile(String id) throws LackOfUserException {
        return UserRepository.getInstance().getById(id);
    }

    public void increaseCredit(String extraCredit) throws LackOfUserException {
        if (UserRepository.getInstance().size() <= loggedInUserIndex )
            throw new LackOfUserException();
        User user = UserRepository.getInstance().getByIndex(loggedInUserIndex);
        user.increaseCredit(extraCredit);
        UserRepository.getInstance().update(user);
    }

    public void addRestaurant(String restaurantInfoString) throws JsonProcessingException, DuplicateRestaurantException {
        RestaurantInfo newRestaurant;
        newRestaurant = mapper.readValue(restaurantInfoString, RestaurantInfo.class);
        newRestaurant.setRegularMenuFromMenu();
        RestaurantRepository.getInstance().add(newRestaurant);
    }

    public void addFood(String foodInfoString) throws JsonProcessingException, DuplicateFoodException, LackOfRestaurantException {
        FoodInfo newFood;
        newFood = mapper.readValue(foodInfoString, FoodInfo.class);
        for (RestaurantInfo restaurant : RestaurantRepository.getInstance().getAll())
        {
            if(restaurant.getName().equals(newFood.getRestaurantName()))
            {
                for(RestaurantFoodInfo restaurantFoodInfo : restaurant.getRegularMenu())
                    if(restaurantFoodInfo.getName().equals(newFood.getName()))
                        throw new DuplicateFoodException();
                RestaurantFoodInfo food = convertFoodToRestaurantFood(newFood);
                restaurant.getRegularMenu().add(food);
                RestaurantRepository.getInstance().update(restaurant); /// isn't it by reference? do we really need update method?
                return;
            }
        }
        throw new LackOfRestaurantException();
    }

    public RestaurantFoodInfo convertFoodToRestaurantFood(FoodInfo food){
        RestaurantFoodInfo restaurantFood = new RestaurantFoodInfo();
        restaurantFood.setName(food.getName());
        restaurantFood.setDescription(food.getDescription());
        restaurantFood.setPopularity(food.getPopularity());
        restaurantFood.setPrice(food.getPrice());
        return restaurantFood;
    }

    public List<RestaurantViewModel> getRestaurants() throws LackOfAnyRestaurantException {
        List<RestaurantInfo> restaurants = RestaurantRepository.getInstance().getAll();
        if (restaurants.isEmpty())
            throw new LackOfAnyRestaurantException();
        List<RestaurantViewModel> restaurantViewModels = new ArrayList<>();
        for (int i = 0; i < restaurants.size(); i++) {
            RestaurantInfo restaurant = restaurants.get(i);
            if (Math.sqrt(pow(restaurant.getLocation().getX() -
                    UserRepository.getInstance().getByIndex(loggedInUserIndex).getLocation().getX(), 2)
                    + pow(restaurant.getLocation().getY() -
                    UserRepository.getInstance().getByIndex(loggedInUserIndex).getLocation().getY(), 2)) > MAXIMUM_DISTANCE_ALLOWED)
                continue;
            RestaurantViewModel model = new RestaurantViewModel();
            model.id = restaurant.getId();
            model.name = restaurant.getName();
            model.logo = restaurant.getLogo();
            model.location = restaurant.getLocation();
            restaurantViewModels.add(model);
        }
        return restaurantViewModels;
    }

    public RestaurantInfo getRestaurant(String id) throws LackOfRestaurantException, FurtherThanAllowedException {
        RestaurantInfo restaurant = RestaurantRepository.getInstance().getById(id);
        if(Math.sqrt(pow(restaurant.getLocation().getX() - UserRepository.getInstance().getByIndex(loggedInUserIndex).getLocation().getX(), 2)
                + pow(restaurant.getLocation().getY() - UserRepository.getInstance().getByIndex(loggedInUserIndex).getLocation().getY(), 2)) <= MAXIMUM_DISTANCE_ALLOWED)
            return restaurant;
        else
            throw new FurtherThanAllowedException();
    }

    public String getFood(String intendedFoodInfoString) throws JsonProcessingException, LackOfFoodException, LackOfRestaurantException {
        IntendedFoodInfo intendedFood;
        intendedFood = mapper.readValue(intendedFoodInfoString, IntendedFoodInfo.class);
        for (RestaurantInfo restaurant : RestaurantRepository.getInstance().getAll())
            if (restaurant.getName().equals(intendedFood.getRestaurantName())) {
                for (RestaurantFoodInfo food : restaurant.getRegularMenu())
                    if (food.getName().equals((intendedFood.getFoodName())))
                        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(food) + "\n";
                throw new LackOfFoodException();
            }
        throw new LackOfRestaurantException();
    }

    public static TreeMap<String, Float> sortMapByValue(HashMap<String, Float> map){
        Comparator<String> comparator = new ValueComparator(map);
        TreeMap<String, Float> result = new TreeMap<>(comparator);
        result.putAll(map);
        return result;
    }

    public String getRecommendedRestaurants() throws LackOfAnyRestaurantException {
        HashMap<String, Float> restaurantPopulation = new HashMap<>();
        StringBuilder recommendedRestaurants = new StringBuilder();
        if (RestaurantRepository.getInstance().getAll().isEmpty())
            throw new LackOfAnyRestaurantException();
        for (RestaurantInfo restaurant : RestaurantRepository.getInstance().getAll())
            restaurantPopulation.put(restaurant.getName(), calculateRestaurantPopulation(restaurant));
        TreeMap<String, Float> sortedRestaurantPopulation = sortMapByValue(restaurantPopulation);
        int numOfRecommendedRestaurant = 0;
        for (Map.Entry<String, Float> entry : sortedRestaurantPopulation.entrySet())
        {
            recommendedRestaurants.append(entry.getKey()).append("\n");
            numOfRecommendedRestaurant++;
            if (numOfRecommendedRestaurant == 3)
                return recommendedRestaurants.toString();
        }
        return recommendedRestaurants.toString();
    }

    public Float calculateRestaurantPopulation(RestaurantInfo restaurant){
        float averageFoodsPopulation = 0;
        float restaurantPopulation;
        for(RestaurantFoodInfo food : restaurant.getRegularMenu()){
            averageFoodsPopulation = food.getPopularity() + averageFoodsPopulation;
        }
        averageFoodsPopulation = averageFoodsPopulation / restaurant.getRegularMenu().size();
        restaurantPopulation = (float) (averageFoodsPopulation/ Math.sqrt(pow(restaurant.getLocation().getX() - UserRepository.getInstance().getByIndex(loggedInUserIndex).getLocation().getX(), 2)
                + pow(restaurant.getLocation().getY() - UserRepository.getInstance().getByIndex(loggedInUserIndex).getLocation().getY(), 2)));
        return  restaurantPopulation;
    }

    public void addToCart(IntendedFoodInfo intendedFoodInfo) throws UnfinishedOrderException, LackOfFoodException, LackOfRestaurantException, DoseNotExistException {
        if( !orderOnGoingRestaurant.equals("") && !orderOnGoingRestaurant.equals(intendedFoodInfo.getRestaurantName()))
            throw new UnfinishedOrderException();
        boolean orderStatus = false;
        for (OrderedFoodInfo orderedFoodInfo : cart) {
            if(orderedFoodInfo.getFoodName().equals(intendedFoodInfo.getFoodName()) && (orderedFoodInfo.isRegular() == intendedFoodInfo.isRegular()) && orderedFoodInfo.isRegular()){
                orderedFoodInfo.setOrdersNumber(orderedFoodInfo.getOrdersNumber() + 1);
                return;
            }
        }

        List<RestaurantInfo> restaurants = RestaurantRepository.getInstance().getAll();

        for (int i = 0; i < restaurants.size(); i++)
        {
            if(restaurants.get(i).getName().equals(intendedFoodInfo.getRestaurantName())) {
                if (!intendedFoodInfo.isRegular()) {
                    List<DiscountedFood> foodPartyMenu = restaurants.get(i).getFoodPartyMenu();
                    for (int i1 = 0; i1 < foodPartyMenu.size(); i1++) {
                        DiscountedFood food = foodPartyMenu.get(i1);
                        if (food.getName().equals(intendedFoodInfo.getFoodName()))
                            if (food.getCount() > 0) {
                                food.decreaseCount();
                                for (OrderedFoodInfo orderedFoodInfo : cart) {
                                    if(orderedFoodInfo.getFoodName().equals(intendedFoodInfo.getFoodName()) && (orderedFoodInfo.isRegular() == intendedFoodInfo.isRegular()) && !orderedFoodInfo.isRegular()){
                                        orderedFoodInfo.setOrdersNumber(orderedFoodInfo.getOrdersNumber() + 1);
                                        return;
                                    }
                                }
                                orderStatus = true;
                                break;
                            } else
                                throw new DoseNotExistException();
                    }
                }else{
                    for (RestaurantFoodInfo food : restaurants.get(i).getRegularMenu())
                        if (food.getName().equals((intendedFoodInfo.getFoodName()))) {
                            orderStatus = true;
                            break;
                        }
                }
                if (orderStatus) {
                    OrderedFoodInfo orderedFoodInfo = new OrderedFoodInfo();
                    orderedFoodInfo.setFoodName(intendedFoodInfo.getFoodName());
                    orderedFoodInfo.setRegular(intendedFoodInfo.isRegular());
                    orderedFoodInfo.setOrdersNumber(1);
                    orderOnGoingRestaurant = intendedFoodInfo.getRestaurantName();
                    cart.add(orderedFoodInfo);
                    return;
                }
                throw new LackOfFoodException();
            }
        }
        throw new LackOfRestaurantException();
    }

    public List<OrderedFoodInfo> getCart() throws EmptyCartException {
        if(cart.isEmpty())
            throw new EmptyCartException();
        return cart;
    }

    public void finalizeOrder() throws EmptyCartException, NotEnoughCreditException, LackOfFoodException, LackOfRestaurantException {
        if(cart.isEmpty())
            throw new EmptyCartException();
        int cartCost = calculateCartCost();
        if (!checkEnoughCredit(cartCost))
            throw new NotEnoughCreditException();
        User loggedInUser = UserRepository.getInstance().getByIndex(loggedInUserIndex);
        loggedInUser.setCredit(loggedInUser.getCredit() - cartCost);
        List<OrderedFoodInfo> orderedFoods = new ArrayList<>(cart);
        Order newOrder = new Order(orderedFoods, loggedInUser, RestaurantRepository.getInstance().getByName(orderOnGoingRestaurant));
        OrderRepository.getInstance().addOrUpdate(newOrder);
        (new AssignDeliveryToOrders()).run();
        cart.removeAll(cart);
        orderOnGoingRestaurant = "";
    }

    private int calculateCartCost() throws LackOfFoodException, LackOfRestaurantException {
        int cost = 0;
        for (OrderedFoodInfo orderedFoodInfo : cart)
            cost += orderedFoodInfo.getOrdersNumber() * findFoodPrice(orderedFoodInfo);
        return cost;
    }

    private boolean checkEnoughCredit(int cost){
        return UserRepository.getInstance().getByIndex(loggedInUserIndex).getCredit() >= cost;
    }

    private int findFoodPrice(OrderedFoodInfo orderedFood) throws LackOfFoodException, LackOfRestaurantException {
        if (orderedFood.isRegular()) {
            List<RestaurantFoodInfo>regularMenu = RestaurantRepository.getInstance().getByName(orderOnGoingRestaurant).getRegularMenu();
            for (RestaurantFoodInfo food : regularMenu){
                if(food.getName().equals(orderedFood.getFoodName()))
                    return food.getPrice();
            }
        } else {
            List<DiscountedFood> foodPartyMenu = RestaurantRepository.getInstance().getByName(orderOnGoingRestaurant).getFoodPartyMenu();
            for (DiscountedFood food : foodPartyMenu) {
                System.out.println(food.getName());
                System.out.println(orderedFood.getFoodName());
                if (food.getName().equals(orderedFood.getFoodName()))
                    return food.getPrice();
            }
        }
        throw new LackOfFoodException();
    }

}
