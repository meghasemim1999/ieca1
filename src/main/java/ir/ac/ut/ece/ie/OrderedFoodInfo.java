package ir.ac.ut.ece.ie;

public class OrderedFoodInfo {
    private String foodName;
    private int ordersNumber;
    private boolean isRegular;

    public OrderedFoodInfo(){
        ordersNumber = 0;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getOrdersNumber() {
        return ordersNumber;
    }

    public void setOrdersNumber(int ordersNumber) {
        this.ordersNumber = ordersNumber;
    }

    public boolean isRegular() {
        return isRegular;
    }

    public void setRegular(boolean regular) {
        isRegular = regular;
    }
}
