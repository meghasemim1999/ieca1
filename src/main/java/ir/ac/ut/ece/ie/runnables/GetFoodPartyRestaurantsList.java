package ir.ac.ut.ece.ie.runnables;

import ir.ac.ut.ece.ie.RestaurantInfo;
import ir.ac.ut.ece.ie.exceptions.DuplicateRestaurantException;
import ir.ac.ut.ece.ie.exceptions.LackOfRestaurantException;
import ir.ac.ut.ece.ie.repositories.RestaurantRepository;

import java.util.List;

public class GetFoodPartyRestaurantsList implements Runnable
{
    @Override
    public void run() {
        RestaurantRepository.getInstance().clearFoodPartyMenu();
        List<RestaurantInfo> foodPartyRestaurants = RestaurantRepository.getInstance().getFoodPartyRestaurantsListFromServer();
        if (foodPartyRestaurants == null)
            return;
        for (RestaurantInfo foodPartyRestaurant  : foodPartyRestaurants) {
            foodPartyRestaurant.setFoodPartyMenuFromMenu();
            if(RestaurantRepository.getInstance().existsById(foodPartyRestaurant.getId())) {
                try {
                    foodPartyRestaurant.setRegularMenu(RestaurantRepository.getInstance().getById(foodPartyRestaurant.getId()).getRegularMenu());
                    RestaurantRepository.getInstance().update(foodPartyRestaurant);
                } catch (LackOfRestaurantException e) {
                    e.printStackTrace();
                }
            }
            else {
                try {
                    RestaurantRepository.getInstance().add(foodPartyRestaurant);
                } catch (DuplicateRestaurantException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}