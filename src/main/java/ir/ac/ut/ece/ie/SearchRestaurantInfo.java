package ir.ac.ut.ece.ie;

public class SearchRestaurantInfo {
    private String name;

    public SearchRestaurantInfo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
