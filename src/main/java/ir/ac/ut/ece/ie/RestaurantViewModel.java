package ir.ac.ut.ece.ie;

public class RestaurantViewModel {
    public String id;
    public String logo;
    public String name;
    public Location location;
}
