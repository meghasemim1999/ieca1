package ir.ac.ut.ece.ie.controllers;

import ir.ac.ut.ece.ie.LoghmehCore;
import ir.ac.ut.ece.ie.exceptions.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/FinalizeCart")
public class FinalizeCartController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            LoghmehCore.getInstance().finalizeOrder();
        } catch (NotEnoughCreditException notEnoughCreditException) {
            response.sendError(403, "You dont have enough credit to finalize your order.");
            return;
        } catch (EmptyCartException emptyCartException) {
            response.sendError(404, "Cart is empty");
            return;
        } catch(LackOfFoodException e) {
            response.sendError(404, "Chosen food does not exist");
            return;
        } catch (LackOfRestaurantException e) {
            response.sendError(404, "Restaurant does not exist");
            return;
        }
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("finalizeCart.jsp");
        requestDispatcher.forward(request, response);
    }
}
