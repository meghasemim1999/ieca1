package ir.ac.ut.ece.ie;

public class User {
    private int id;
    private String firstname;
    private String lastname;
    private String phoneNumber;
    private String email;
    private long credit;
    private Location location;

    private static int LAST_ASSIGNED_ID = 0;
    private static Location DEFAULT_LOCATION;

    static {
        DEFAULT_LOCATION = new Location();
        DEFAULT_LOCATION.setX(0);
        DEFAULT_LOCATION.setY(0);
    }

    public User(String firstname, String lastname, String phoneNumber, String email, long credit) {
        this.id = LAST_ASSIGNED_ID++;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.credit = credit;
        Location defaultLocation = new Location();
        defaultLocation.setX(0);
        defaultLocation.setY(0);
        this.location = defaultLocation;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public long getCredit() {
        return credit;
    }

    public void setCredit(long credit) {
        this.credit = credit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void increaseCredit(String extraCredit) {
        this.credit += Long.parseLong(extraCredit);
    }
}