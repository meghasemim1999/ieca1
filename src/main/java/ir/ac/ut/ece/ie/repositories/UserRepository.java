package ir.ac.ut.ece.ie.repositories;

import ir.ac.ut.ece.ie.RestaurantInfo;
import ir.ac.ut.ece.ie.User;
import ir.ac.ut.ece.ie.exceptions.LackOfUserException;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private static UserRepository instance;
    private List<User> users;

    private UserRepository(){
        users = new ArrayList<User>();
        users.add(new User ("Asghar", "Asghari", "09123456789", "asgharkopol@gmail.com", 100000));
    }

    public static UserRepository getInstance()
    {
        if(instance == null)
            instance = new UserRepository();

        return instance;
    }

    public int size(){
        return users.size();
    }
    public  User getByIndex(int index){
        return users.get(index);
    }
    public  User getById(String id) throws LackOfUserException {
        for (User user : users)
            if (user.getId() == Integer.parseInt(id))
                return user;
        throw new LackOfUserException();
    }

    public void update(User updatedUser){
        for (int i = 0; i < users.size() ; i++ ){
            if(users.get(i).getId() == updatedUser.getId())
                users.set(i, updatedUser);
        }
    }
}
