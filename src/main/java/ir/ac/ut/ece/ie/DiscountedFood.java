package ir.ac.ut.ece.ie;

public class DiscountedFood extends RestaurantFoodInfo{
    private int count;
    private int oldPrice;

    public int getCount() {
        return this.count;
    }

    public int getOldPrice() {
        return oldPrice;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setOldPrice(int oldPrice) {
        this.oldPrice = oldPrice;
    }

    public void decreaseCount(){
        this.count--;
    }
}


