package ir.ac.ut.ece.ie;

import java.util.List;

public class Order {
    private static int LAST_ASSIGNED_ID = 0;

    private User orderOwner;
    private int id;
    private OrderState orderState;
    private RestaurantInfo orderRestaurant;
    private List<OrderedFoodInfo> orderedFoods;
    private Delivery orderDelivery;

    public Order(List<OrderedFoodInfo> orderedFoodInfos, User orderOwner, RestaurantInfo orderRestaurant)
    {
        this.orderedFoods = orderedFoodInfos;
        this.orderOwner = orderOwner;
        this.orderRestaurant = orderRestaurant;
        id = LAST_ASSIGNED_ID++;
        orderState = OrderState.SEARCHING_FOR_DELIVERY;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public int getId() {
        return id;
    }

    public User getOrderOwner() {
        return orderOwner;
    }

    public RestaurantInfo getOrderRestaurant() {
        return orderRestaurant;
    }

    public List<OrderedFoodInfo> getOrderedFoods() {
        return orderedFoods;
    }

    public enum OrderState {
        SEARCHING_FOR_DELIVERY,
        DELIVERY_ON_THE_WAY,
        DELIVERED
    }
}
