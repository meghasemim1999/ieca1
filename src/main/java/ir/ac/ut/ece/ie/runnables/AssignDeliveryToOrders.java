package ir.ac.ut.ece.ie.runnables;

import ir.ac.ut.ece.ie.Delivery;
import ir.ac.ut.ece.ie.Order;
import ir.ac.ut.ece.ie.repositories.DeliveryRepository;
import ir.ac.ut.ece.ie.repositories.OrderRepository;

import java.util.List;

public class AssignDeliveryToOrders implements Runnable {

    @Override
    public void run() {
        List<Order> orders = OrderRepository.getInstance().getAll();
        for (int i = 0; i< orders.size(); i++) {
            if (orders.get(i).getOrderState() == Order.OrderState.SEARCHING_FOR_DELIVERY) {
                Delivery assignableDelivery = DeliveryRepository.getInstance().searchForBestDelivery(orders.get(i).getOrderOwner().getLocation(),
                        orders.get(i).getOrderRestaurant().getLocation());
                if(assignableDelivery == null)
                    continue;
                assignableDelivery.assignOrder(orders.get(i));
                DeliveryRepository.getInstance().addOrUpdate(assignableDelivery);
            }
        }
    }
}
