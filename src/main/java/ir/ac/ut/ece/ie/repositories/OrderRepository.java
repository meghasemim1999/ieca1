package ir.ac.ut.ece.ie.repositories;

import ir.ac.ut.ece.ie.Delivery;
import ir.ac.ut.ece.ie.Order;
import ir.ac.ut.ece.ie.User;
import ir.ac.ut.ece.ie.exceptions.LackOfOrderException;

import java.util.ArrayList;
import java.util.List;

public class OrderRepository {
    private static OrderRepository instance;

    private List<Order> orders;

    private OrderRepository() {
        orders = new ArrayList<>();
    }

    public static OrderRepository getInstance()
    {
        if(instance == null)
            instance = new OrderRepository();

        return instance;
    }

    public void addOrUpdate(Order newOrder){
        for (int i = 0; i < orders.size(); i++) {
            Order order = orders.get(i);
            if (order.getId() == newOrder.getId()) {
                orders.set(i, newOrder);
                return;
            }
        }
        orders.add(newOrder);
    }

    public List<Order> getAll() {
        return orders;
    }

    public Order getById(int id) throws LackOfOrderException {
        for( Order order : orders){
            if(order.getId() == id)
                return order;
        }
        throw new LackOfOrderException();
    }

    public List<Order> getUserOrders(int userId) {
        List<Order> userOrders = new ArrayList<>();
        for( Order order : orders){
            if(order.getOrderOwner().getId() == userId)
                userOrders.add(order);
        }
        return userOrders;
    }

}
