package ir.ac.ut.ece.ie;

import ir.ac.ut.ece.ie.Scheduler;
import ir.ac.ut.ece.ie.runnables.AssignDeliveryToOrders;
import ir.ac.ut.ece.ie.runnables.GetFoodPartyRestaurantsList;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class BackgroundJobManager implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        Scheduler.getScheduler().scheduleAtFixedRate(new AssignDeliveryToOrders(),
                0,
                30,
                TimeUnit.SECONDS);
        Scheduler.getScheduler().scheduleAtFixedRate(new GetFoodPartyRestaurantsList(),
                10 ,
                600,
                TimeUnit.SECONDS);

    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        Scheduler.getScheduler().shutdownNow();
    }

}
