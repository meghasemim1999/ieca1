package ir.ac.ut.ece.ie.controllers;

import ir.ac.ut.ece.ie.IntendedFoodInfo;
import ir.ac.ut.ece.ie.LoghmehCore;
import ir.ac.ut.ece.ie.exceptions.DoseNotExistException;
import ir.ac.ut.ece.ie.exceptions.LackOfFoodException;
import ir.ac.ut.ece.ie.exceptions.LackOfRestaurantException;
import ir.ac.ut.ece.ie.exceptions.UnfinishedOrderException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@WebServlet("/AddToCart")
public class AddToCartController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        IntendedFoodInfo intendedFoodInfo = new IntendedFoodInfo();
        intendedFoodInfo.setRestaurantName(request.getParameter("RestaurantName"));
        intendedFoodInfo.setFoodName(request.getParameter("FoodName"));
        intendedFoodInfo.setRegular(request.getParameter("isRegular"));
        try {
            LoghmehCore.getInstance().addToCart(intendedFoodInfo);
        } catch (UnfinishedOrderException e) {
            response.sendError(403,"You have another order going on.");
            return;
        } catch (LackOfFoodException e) {
            response.sendError(404,"Chosen food does not exist.");
            return;
        } catch (LackOfRestaurantException e) {
            response.sendError(404,"Chosen restaurant does not exist.");
            return;
        } catch (DoseNotExistException e) {
            response.sendError(403, "Selected food have finished we are sorry :(");
            return;
        }
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("addToCart.jsp");
        requestDispatcher.forward(request,response);
    }
}