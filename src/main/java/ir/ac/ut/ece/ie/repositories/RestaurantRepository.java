package ir.ac.ut.ece.ie.repositories;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.ie.RestaurantInfo;
import ir.ac.ut.ece.ie.Scheduler;
import ir.ac.ut.ece.ie.exceptions.DuplicateRestaurantException;
import ir.ac.ut.ece.ie.exceptions.LackOfRestaurantException;
import ir.ac.ut.ece.ie.runnables.GetFoodPartyRestaurantsList;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RestaurantRepository {
    private static volatile RestaurantRepository instance;
    private static boolean isInstanceCreationStarted = false;

    private List<RestaurantInfo> restaurants;
    private ObjectMapper mapper;

    private RestaurantRepository() {
        mapper = new ObjectMapper();
        restaurants = getRestaurantsListFromServer();
        if (!restaurants.isEmpty())
            for (RestaurantInfo restaurant : restaurants)
                restaurant.setRegularMenuFromMenu();
    }

    public static void main(String[] args) {
        List<RestaurantInfo> restaurantInfos = RestaurantRepository.getInstance().getFoodPartyRestaurantsListFromServer();

    }

    public static RestaurantRepository getInstance()
    {
        if(isInstanceCreationStarted)
            while (instance == null) {
                Thread.onSpinWait();
            }

        if(instance == null) {
            isInstanceCreationStarted = true;
            instance = new RestaurantRepository();
            isInstanceCreationStarted = false;
        }

        return instance;
    }

    private List<RestaurantInfo> getRestaurantsListFromServer()
    {
        HttpResponse<String> response = Unirest.get("http://138.197.181.131:8080/restaurants")
                .header("accept", "application/json")
                .asString();
        try {
//            return mapper.readValue(response.getBody(), new TypeReference<>() {});
            return mapper.readValue(response.getBody(), new TypeReference<>() {});

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<RestaurantInfo> getFoodPartyRestaurantsListFromServer()
    {
        HttpResponse<String> response = Unirest.get("http://138.197.181.131:8080/foodparty")
                .header("accept", "application/json")
                .asString();
        try {
            return mapper.readValue(response.getBody(), new TypeReference<>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void clearFoodPartyMenu(){
        for (int i = 0; i < restaurants.size(); i++) {
            RestaurantInfo restaurant = restaurants.get(i);
            if (restaurant.getFoodPartyMenu() != null)
                restaurant.clearFoodPartyMenu();
        }
    }

    public RestaurantInfo getById(String id) throws LackOfRestaurantException {
        for (RestaurantInfo restaurant : RestaurantRepository.getInstance().getAll())
            if (restaurant.getId().equals(id))
                return restaurant;

        throw new LackOfRestaurantException();
    }

    public RestaurantInfo getByName(String name) throws LackOfRestaurantException {
        List<RestaurantInfo> all = RestaurantRepository.getInstance().getAll();
        for (int i = 0; i < all.size(); i++) {
            RestaurantInfo restaurant = all.get(i);
            if (restaurant.getName().equals(name))
                return restaurant;
        }

        throw new LackOfRestaurantException();
    }

    public boolean existsById(String id) {
        for (RestaurantInfo restaurant : RestaurantRepository.getInstance().getAll())
            if (restaurant.getId().equals(id))
                return true;

        return false;
    }

    public void add(RestaurantInfo newRestaurant) throws DuplicateRestaurantException {
        for (RestaurantInfo restaurant : restaurants)
            if (restaurant.getName().equals(newRestaurant.getName()))
                throw new DuplicateRestaurantException();

        restaurants.add(newRestaurant);
    }

    public void update(RestaurantInfo updatedRestaurant){
        for (int i = 0; i < restaurants.size() ; i++ ){
            if(restaurants.get(i).getId().equals(updatedRestaurant.getId()))
                restaurants.set(i, updatedRestaurant);
        }
    }

    public List<RestaurantInfo> getAll(){
        return restaurants;
    }
}
