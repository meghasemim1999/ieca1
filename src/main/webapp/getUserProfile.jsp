<%@ page import="ir.ac.ut.ece.ie.User" %>
<%@ page import="ir.ac.ut.ece.ie.LoghmehCore" %>
<%@ page import="ir.ac.ut.ece.ie.exceptions.LackOfUserException" %>
<%@ page import="ir.ac.ut.ece.ie.Order" %>
<%@ page import="ir.ac.ut.ece.ie.repositories.OrderRepository" %><%--
  Created by IntelliJ IDEA.
  User: Fatemeh
  Date: 2/23/2020
  Time: 11:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    User user;
    try {
        user = LoghmehCore.getInstance().getUserProfile(request.getParameter("id"));
    } catch (LackOfUserException lackOfUserException) {
        return;
    }
%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>User</title>
    <style>
        li {
            padding: 5px
        }
    </style>
</head>

<body>
<ul>
    <li><%= "id: " + user.getId()%></li>
    <li><%= "full name: " + user.getFirstname() + " " + user.getLastname()%></li>
    <li><%= "phone number: " + user.getPhoneNumber()%></li>
    <li><%= "email: " + user.getEmail()%></li>
    <li><%= "credit: " + user.getCredit() + " Toman"%></li>
    <form action="IncreaseCredit" method="POST">
        <button type="submit">increase</button>
        <input type="text" name="extraCredit" value="" />
        <input type="hidden" name="id" value=<%=user.getId()%> />
    </form>
    <li>
        Orders :
        <ul>
            <%
                for (Order order : OrderRepository.getInstance().getUserOrders(user.getId()))
                {
            %>
            <li>
                <a href="GetOrderStatus?id=<%=order.getId()%>">order id : <%=order.getId()%></a>
            </li>
            <%}%>
        </ul>
    </li>
</ul>
</body>

</html>