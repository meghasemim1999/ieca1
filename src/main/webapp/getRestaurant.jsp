<%@ page import="ir.ac.ut.ece.ie.RestaurantInfo" %>
<%@ page import="ir.ac.ut.ece.ie.LoghmehCore" %>
<%@ page import="ir.ac.ut.ece.ie.exceptions.FurtherThanAllowedException" %>
<%@ page import="ir.ac.ut.ece.ie.exceptions.LackOfRestaurantException" %>
<%@ page import="ir.ac.ut.ece.ie.RestaurantFoodInfo" %>
<%@ page import="java.util.List" %>
<%@ page import="ir.ac.ut.ece.ie.DiscountedFood" %><%--
  Created by IntelliJ IDEA.
  User: Fatemeh
  Date: 2/22/2020
  Time: 10:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Restaurant Info</title>
    <style>
        img {
            width: 50px;
            height: 50px;
        }
        li {
            display: flex;
            flex-direction: row;
            padding: 0 0 5px;
        }
        div, form {
            padding: 0 5px
        }
    </style>
</head>
<body>
    <%
        RestaurantInfo restaurantInfo = null;
        try {
            restaurantInfo = LoghmehCore.getInstance().getRestaurant(request.getParameter("id"));
        } catch (FurtherThanAllowedException furtherThanAllowedException) {
            response.sendError(403, "Restaurant is further than maximum distance.");
            return;
        } catch (LackOfRestaurantException furtherThanAllowedException) {
            response.sendError(404,"Restaurant does not exist.");
            return;
        }
    %>
    <ul>
        <li><%="id:"  + restaurantInfo.getId()%></li>
        <li><%= "name: " + restaurantInfo.getName()%></li>
        <li><%="location: (" + restaurantInfo.getLocation().getX() + ", " + restaurantInfo.getLocation().getY() + ")"%></li>
        <li>logo: <img alt="logo" src=<%=restaurantInfo.getLogo()%> ></li>

<%--        <!-- IN CASE YOU WANT SOME BONUS : -->--%>
<%--        <!-- <li>estimated delivery time: 10 min 2 sec </li> -->--%>

        <li>menu:
            <ul>

<%
                List<RestaurantFoodInfo> restaurantFoodInfos = restaurantInfo.getRegularMenu();
                if(restaurantFoodInfos == null || restaurantFoodInfos.isEmpty())
                    return;
                for (RestaurantFoodInfo restaurantFoodInfo : restaurantFoodInfos) {%>
                <li>
                    <img alt="logo" src=<%=restaurantFoodInfo.getImage()%>>
                    <div><%=restaurantFoodInfo.getName()%></div>
                    <div><%=restaurantFoodInfo.getPrice() + "Tooman"%></div>
                    <form action="AddToCart" method="POST">
                        <button type="submit">addToCart</button>
                        <%= restaurantInfo.getName()%>
                        <input type="hidden" name="RestaurantName" value="<%= restaurantInfo.getName()%>" />
                        <input type="hidden" name="FoodName" value="<%= restaurantFoodInfo.getName()%>" />
                        <input type="hidden" name="isRegular" value="true" />
                    </form>
                </li>
                <%}%>
            </ul>
        </li>
    </ul>
</body>
</html>
