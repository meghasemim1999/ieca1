<%@ page import="ir.ac.ut.ece.ie.Order" %>
<%@ page import="ir.ac.ut.ece.ie.repositories.OrderRepository" %>
<%@ page import="ir.ac.ut.ece.ie.exceptions.LackOfOrderException" %>
<%@ page import="ir.ac.ut.ece.ie.OrderedFoodInfo" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: ebi
  Date: 24.02.20
  Time: 11:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    Order order = null;
    try {
        order = OrderRepository.getInstance().getById(Integer.parseInt(request.getParameter("id")));
    } catch (LackOfOrderException e) {
        response.sendError(404, "Order not found.");
    }
%>


<html>
<head>
    <title>Order <%=order.getId()%></title>
</head>
<body>
    <%=order.getOrderOwner().getFirstname()%>
    <%=order.getOrderOwner().getLastname() %>
    <br>
    <%= "\n" + order.getOrderState().toString() + "\n" %>
    <br>
    <%=order.getOrderRestaurant().getName()%>

    <br>
    <%
        List<OrderedFoodInfo>foods = order.getOrderedFoods();
        for (OrderedFoodInfo food : foods)
        {
    %>
        <%= food.getFoodName() + ": " + food.getOrdersNumber() + "\n"  %>
    <br>
    <%  } %>
</body>
</html>
