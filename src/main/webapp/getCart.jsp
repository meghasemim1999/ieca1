<%@ page import="ir.ac.ut.ece.ie.OrderedFoodInfo" %>
<%@ page import="java.util.List" %>
<%@ page import="ir.ac.ut.ece.ie.LoghmehCore" %>
<%@ page import="ir.ac.ut.ece.ie.exceptions.EmptyCartException" %><%--
  Created by IntelliJ IDEA.
  User: Fatemeh
  Date: 2/24/2020
  Time: 10:58 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cart</title>
    <style>
        li, div, form {
            padding: 5px
        }
    </style>
</head>
<%
    List<OrderedFoodInfo> orderedFoods;
    try {
        orderedFoods = LoghmehCore.getInstance().getCart();
    } catch (EmptyCartException e) {
        response.sendError(404, "Cart is empty");
        return;
    }
    int orderNum = 0;

%>
<body>
<div>restaurant name</div>
<%
    for (OrderedFoodInfo orderedFood : orderedFoods){
        if(orderedFood.isRegular()){
            orderNum++;
%>
<ul>
    <li><%=orderedFood.getFoodName() + ": " + orderedFood.getOrdersNumber()%></li>
</ul>
<%}
    }
    if (orderNum < orderedFoods.size()){
%>

<div>FoodParty</div>
<%
    for (OrderedFoodInfo orderedFood : orderedFoods){
%>
<ul>
    <li><%=orderedFood.isRegular() ? "" : orderedFood.getFoodName() + ": " + orderedFood.getOrdersNumber()%></li>
</ul>
<%}}%>
<form action="FinalizeCart" method="POST">
    <button type="submit">finalize</button>
</form>
</body>
</html>
