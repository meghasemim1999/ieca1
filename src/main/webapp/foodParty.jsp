<%--
  Created by IntelliJ IDEA.
  User: Fatemeh
  Date: 2/25/2020
  Time: 2:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="ir.ac.ut.ece.ie.DiscountedFood" %>
<%@ page import="ir.ac.ut.ece.ie.RestaurantInfo" %>
<%@ page import="ir.ac.ut.ece.ie.repositories.RestaurantRepository" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: ebi
  Date: 22.02.20
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Food Party</title>
    <style>
        img {
            width: 50px;
            height: 50px;
        }
        li {
            display: flex;
            flex-direction: row;
            padding: 0 0 5px;
        }
        div, form {
            padding: 0 5px
        }
        .old-price {
            text-decoration: line-through;
        }
    </style>
</head>
<body>
<ul>
    <%
    List<RestaurantInfo> restaurants;
    List<DiscountedFood> discountedFoods;
    restaurants = RestaurantRepository.getInstance().getAll();
        for (int i = 0; i < restaurants.size(); i++) {
            RestaurantInfo restaurant = restaurants.get(i);
            discountedFoods = restaurant.getFoodPartyMenu();
            if (discountedFoods == null || discountedFoods.isEmpty())
                continue;
            for (int i1 = 0; i1 < discountedFoods.size(); i1++) {
                DiscountedFood food = discountedFoods.get(i1);
    %>
    <li> menu:
        <ul>
            <li>
                <img alt="logo" src=<%=food.getImage() %>>
                <div> <%=restaurant.getName() %>> </div>
                <div><%=food.getName() %></div >
                <div><%=food.getDescription() %></div >
                <div class="old-price" ><%=food.getOldPrice() %> Toman </div >
                <div><%=food.getPrice() %> Toman </div >
                <div> Remaining count: <%=food.getCount() %></div >
                <div> popularity: <%=food.getPopularity() %></div >
                <form action = "AddToCart" method = "POST" >
                    <input type = "hidden" name = "RestaurantName" value = "<%= restaurant.getName() %>"/>
                    <input type = "hidden" name = "FoodName" value = "<%= food.getName() %>"/>
                    <input type = "hidden" name="isRegular" value="false"/>
                    <button type="submit"> addToCart</button>
                </form >
            </li>
        </ul>
    </li>
    <%
            }
        }
    %>

</ul>
</body>
</html>