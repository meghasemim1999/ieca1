<%@ page import="ir.ac.ut.ece.ie.repositories.RestaurantRepository" %>
<%@ page import="ir.ac.ut.ece.ie.RestaurantViewModel" %>
<%@ page import="java.util.List" %>
<%@ page import="ir.ac.ut.ece.ie.LoghmehCore" %>
<%@ page import="ir.ac.ut.ece.ie.exceptions.LackOfAnyRestaurantException" %><%--
  Created by IntelliJ IDEA.
  User: ebi
  Date: 22.02.20
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Restaurants List</title>
    <style>
        table {
            text-align: center;
            margin: auto;
        }
        th, td {
            padding: 5px;
            text-align: center;
        }
        .logo{
            width: 100px;
            height: 100px;
        }
    </style>
</head>
<body>
<table>
    <tr>
        <th>id</th>
        <th>logo</th>
        <th>name</th>
        <th>location</th>
    </tr>
    <%
        int restaurantNo = 1;
        List<RestaurantViewModel> restaurants = null;
        try {
            restaurants = LoghmehCore.getInstance().getRestaurants();
        } catch (LackOfAnyRestaurantException e) {
            response.sendError(404, "There is no restaurant in Loghme right now.");
        }
        if (restaurants != null)
            for(RestaurantViewModel restaurant : restaurants){%>
    <tr>
        <td><%=restaurantNo++%></td>
        <td><img class="logo" alt="logo" src=<%=restaurant.logo%>></td>
        <td><a href="GetRestaurant?id=<%=restaurant.id%>"><%=restaurant.name%></a></td>
        <td><%="(" + restaurant.location.getX() + ", " + restaurant.location.getY() + ")"%></td>
    </tr>
    <%  }%>
</table>
</body>
</html>
