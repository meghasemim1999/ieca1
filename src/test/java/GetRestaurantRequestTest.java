//import ir.ac.ut.ece.ie.LoghmehServer;
//import kong.unirest.HttpResponse;
//import kong.unirest.Unirest;
//import org.junit.*;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//public class GetRestaurantRequestTest {
//    static LoghmehServer server;
//    static int port = 8081;
//
//    @BeforeClass
//    public static void setUp() {
//        server = new LoghmehServer(true);
//        server.startServer(port);
//    }
//
//    @Test
//    public void getCommonRestaurantRequestTest()
//    {
//        HttpResponse<String> response = Unirest.get("http://localhost:" + port + "/Restaurants/1")
//                .asString();
//
//        assertEquals(200 , response.getStatus());
//    }
//
//    @Test
//    public void getFarRestaurantRequestTest()
//    {
//        HttpResponse<String> response = Unirest.get("http://localhost:" + port + "/Restaurants/2")
//                .asString();
//
//        assertEquals(403 , response.getStatus());
//    }
//
//    @Test
//    public void getUnavailableRestaurantRequestTest()
//    {
//        HttpResponse<String> response = Unirest.get("http://localhost:" + port + "/Restaurants/5")
//                .asString();
//
//        assertEquals(404 , response.getStatus());
//    }
//
//    @AfterClass
//    public static void TearDown() throws InterruptedException {
//        server.stopServer();
//
//        // wait for server to stop completely
//        Thread.sleep(2000);
//    }
//}
