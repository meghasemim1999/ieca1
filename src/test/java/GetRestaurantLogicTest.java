//import ir.ac.ut.ece.ie.exceptions.FurtherThanAllowedException;
//import ir.ac.ut.ece.ie.exceptions.LackOfRestaurantException;
//import ir.ac.ut.ece.ie.LoghmehCore;
//import ir.ac.ut.ece.ie.RestaurantInfo;
//import org.junit.Before;
//import org.junit.Test;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//public class GetRestaurantLogicTest {
//
//    LoghmehCore core;
//
//    @Before
//    public void setUp() {
//        core = new LoghmehCore(true);
//    }
//
//    @Test
//    public void getCommonRestaurantLogicTest() throws LackOfRestaurantException, FurtherThanAllowedException {
//        RestaurantInfo restaurantInfo = core.getRestaurant("1");
//        assertNotNull(restaurantInfo.getName());
//        assertNotNull(restaurantInfo.getLocation());
//        assertNotNull(restaurantInfo.getMenu());
//        assertNotNull(restaurantInfo.getId());
//        assertNotNull(restaurantInfo.getLogo());
//    }
//
//    @Test(expected = LackOfRestaurantException.class)
//    public void getFarRestaurantLogicTest() throws LackOfRestaurantException, FurtherThanAllowedException {
//        RestaurantInfo restaurantInfo = core.getRestaurant("5");
//    }
//
//    @Test(expected = FurtherThanAllowedException.class)
//    public void getUnavailableRestaurantLogicTest() throws LackOfRestaurantException, FurtherThanAllowedException {
//        RestaurantInfo restaurantInfo = core.getRestaurant("2");
//    }
//
//}
