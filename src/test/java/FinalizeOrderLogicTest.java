//import ir.ac.ut.ece.ie.IntendedFoodInfo;
//import ir.ac.ut.ece.ie.LoghmehCore;
//import ir.ac.ut.ece.ie.exceptions.*;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//public class FinalizeOrderLogicTest {
//    static LoghmehCore core;
//    static IntendedFoodInfo food;
//
//    @BeforeClass
//    public static void setUp() {
//        core = new LoghmehCore(true);
//        food = new IntendedFoodInfo();
//    }
//
//    @Test(expected = EmptyCartException.class)
//    public void finalizeOrderEmptyCartTest() throws NotEnoughCreditException, LackOfFoodException, InvalidOrderOnGoingRestaurantException, EmptyCartException {
//        core.finalizeOrder();
//    }
//
//    @Test(expected = EmptyCartException.class)
//    public void finalizeOrderCommonCartTest() throws LackOfRestaurantException, UnfinishedOrderException, LackOfFoodException, NotEnoughCreditException, EmptyCartException, InvalidOrderOnGoingRestaurantException {
//        food.setFoodName("KooKoo");
//        food.setRestaurantName("r1");
//        core.addToCart(food);
//        core.finalizeOrder();
//        core.getCart();
//    }
//
//    @Test(expected = NotEnoughCreditException.class)
//    public void finalizeOrderExpensiveCartTest() throws LackOfRestaurantException, UnfinishedOrderException, LackOfFoodException, NotEnoughCreditException, EmptyCartException, InvalidOrderOnGoingRestaurantException {
//        food.setFoodName("KooKoo");
//        food.setRestaurantName("r1");
//        core.addToCart(food);
//        food.setFoodName("KooKoo");
//        food.setRestaurantName("r1");
//        core.addToCart(food);
//        core.finalizeOrder();
//    }
//}
